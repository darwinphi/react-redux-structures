import React, { Component } from 'react'

// class User extends Component {
// 	constructor(props) {
// 		super(props)
// 	}

// 	render() {
// 		return (
// 			<div>
// 				<h1>Hello {this.props.name}</h1>
// 			</div>
// 		)
// 	}
// }

const User = ({name}) => {
	return (
		<div>
			<h1>Hello {name}</h1>
		</div>
	)
} 

export default User