import { createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'

import mathReducer from './reducers/mathReducer'
import userReducer from './reducers/userReducer'

const myLogger = (store) => (next) => (action) => {
	console.log("Logged Action: ", action)
	next(action)
}

const store = createStore(combineReducers({mathReducer, userReducer}), {}, applyMiddleware(myLogger, logger))

export default store