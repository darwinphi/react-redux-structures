import React from 'react'
import ReactDOM from 'react-dom'
import App from './containers/App'

import { createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'

import { Provider } from 'react-redux'

const initialState = {
	result: 1,
	lastValues: []
}

const mathReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'ADD':
				state = {
					...state,
					result: state.result + action.payload,
					lastValues: [...state.lastValues, action.payload]
				}
			break
		case 'SUBTRACT':
				state = {
					...state,
					result: state.result - action.payload,
					lastValues: [...state.lastValues, action.payload]
				}
			break
	}
	return state
}

const userReducer = (state = {
	name: 'Darwin', age: 22
}, action) => {
	switch (action.type) {
		case 'SET_NAME':
				state = {
					...state,
					name: action.payload
				}
			break
		case 'SET_AGE':
				state = {
					...state,
					age: action.payload
				}
			break
	}
	return state
}

const myLogger = (store) => (next) => (action) => {
	console.log("Logged Action: ", action)
	next(action)
}

// const store = createStore(reducer)
const store = createStore(combineReducers({mathReducer, userReducer}), {}, applyMiddleware(myLogger, logger))

store.subscribe(() => {
	// console.log("New state", store.getState())
})

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('app')
)