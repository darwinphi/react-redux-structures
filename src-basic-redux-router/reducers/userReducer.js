const userReducer = (state = {
	name: 'Darwin', age: 22
}, action) => {
	switch (action.type) {
		// We changed it to fulfilled so that promise can woek
		case 'SET_NAME_FULFILLED':
				state = {
					...state,
					name: action.payload
				}
			break
		case 'SET_AGE':
				state = {
					...state,
					age: action.payload
				}
			break
	}
	return state
}

export default userReducer